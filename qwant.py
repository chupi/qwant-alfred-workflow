import sys
import urllib2
import json
import os
from workflow import Workflow, ICON_WEB

WORKFLOW_MODE = bool(True)
QWANT_BASE_URL = 'http://api.qwant.com/api/search/web?count={number}&q={query}&t=web&uiv=4&locale=fr_FR'
HDR = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:75.0) Gecko/20100101 Firefox/75.0', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'Accept-Charset': 'ISO-8859-15,utf-8;q=0.7,*;q=0.3', 'Accept-Encoding': 'none', 'Accept-Language': 'fr-FR,fr;q=0.8', 'Connection': 'keep-alive'}

class SuggestionQwant:
    title = ''
    url = ''

def remove_html_tags(text):
    """Remove html tags from a string"""
    import re
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)

def getSuggestions(query):
    firstchar = query[0]
    url = QWANT_BASE_URL.format(query=query, number = os.getenv('NUM_RESULT', 5))
    req = urllib2.Request(url, headers=HDR)
    reply = urllib2.urlopen(req).read()
    parsed = json.loads(reply)
    result = []
    for qwantResult in parsed['data']['result']['items']:
        suggestion = SuggestionQwant()
        suggestion.title = qwantResult['title']
        suggestion.url = qwantResult['url']
        result.append(suggestion)
    return result

def suggestionsToWorkflow(suggestions):
    debug('Feedback for Alfred:')
    for suggestion in suggestions:
        wf.add_item(title=remove_html_tags(suggestion.title),
                    subtitle=suggestion.url,
                    arg=suggestion.url,
                    valid=str(bool(True)),
                    icon=ICON_WEB)
    wf.send_feedback()

def debug(message):
    if not WORKFLOW_MODE:
        print(message)

def main(wf):
    query = sys.argv[1]
    suggestions = getSuggestions(query)
    suggestionsToWorkflow(suggestions)

if __name__ == '__main__':
    wf = Workflow()
    sys.exit(wf.run(main))