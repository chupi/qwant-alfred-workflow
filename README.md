# Qwant Alfred Workflow

C'est un workflow pour [Alfred](https://www.alfredapp.com/) qui retourne la liste des n premiers résultats de la recherche Qwant

## Installation

Une fois le wokflow installé il faut ajouter [Alfred-Workflow](http://www.deanishe.net/alfred-workflow/), une api python permettant d'interagir avec Alfred comme indiqué dans la documentation

```shell
pip install --target=/path/to/my/workflow Alfred-Workflow
```
